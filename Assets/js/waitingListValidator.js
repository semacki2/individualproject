$(document).ready(function () {
     // reset button
   $("#resetWaitingListBtn").click(function(e){
      $("#partyName").empty();
      $("#partyName").removeClass("errorBorder");
      $("#partyNameError").empty();
      $("#numberInParty").removeClass("errorBorder"); 
      $("#numberInParty").empty();
      $("#numberInPartyError").empty();
   });


    // waiting list submit button validation
    $("#waitingListBtn").click(function (e) {

        var numberInParty = $("#numberInParty").val();
        var partyName = $("#partyName").val();
        var numRegEx = /^[1-9]+$/;
        isValid = false;
        partyNameValid = false;
        numberInPartyValid = false;


        // check for blank partyName field
        if (partyName === '') {
            $("#partyNameError").text("* Party name can't be blank");
            $("#partyName").addClass("errorBorder");
            partyNameValid = false;
        } else {
            $("#partyNameError").empty();
            $("#partyName").removeClass("errorBorder");
            partyNameValid = true;
        }
        
    
        
         // check to make sure numberInParty field is a whole number
        if (numberInParty.match(numRegEx)) {
             $("#numberInPartyError").empty();
             $("#numberInParty").removeClass("errorBorder");
            numberInPartyValid = true;           
        } else {
            $("#numberInPartyError").text("* Please enter number of guests");
            $("#numberInParty").addClass("errorBorder");
            numberInPartyValid = false;
        }      
        
        if (numberInParty.match(numRegEx) && numberInParty <= 6) {
             $("#numberInPartyError").empty();
             $("#numberInParty").removeClass("errorBorder");
            numberInPartyValid = true;           
        } else {
            $("#numberInPartyError").text("* Number in party must be between 1 and 6");
            $("#numberInParty").addClass("errorBorder");
            numberInPartyValid = false;
        } 
        
        if (partyNameValid == true && numberInPartyValid == true) {
            isValid = true;
        }

        // if the form validates submit it
        if (isValid) {
            return true;
        } else {
            return false;
        }
    });

});


