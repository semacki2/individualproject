$(document).ready(function(){
   
   // reset button
   $("#reset").click(function(e){
      $("#date1Error").empty();
      $("#date1").removeClass("errorBorder"); 
      $("#date2Error").empty();
      $("#date2").removeClass("errorBorder");
   });
   
   // submit button validation
   $("#submit").click(function(e){
      
      var date1 = $("#date1").val();
      var date2 = $("#date2").val();
      var isValid = false;
      
      var dateObject1 = new Date(date1);
      var dateObject2 = new Date(date2);
      
      
      // check for blank date1 field
      if (date1 === '') {
        $("#date1Error").text("* Please enter a beginning date");
        $("#date1").addClass("errorBorder");
        isValid = false;        
      }
      else {
        $("#date1Error").empty();
        $("#date1").removeClass("errorBorder");
        isValid = true;
      }
      
      // check for blank date2 field
      if (date2 === '') {
        $("#date2Error").text("* Please enter a ending date");
        $("#date2").addClass("errorBorder");
        isValid = false;
      }
      else {
        $("#date2Error").empty();
        $("#date2").removeClass("errorBorder");
        isValid = true;
      }
      
      // check to see if date1 is before date2
        if (dateObject1 > dateObject2) {
            $("#date1Error").text("* Enter date that falls before ending date");
            $("#date1").addClass("errorBorder");
            isValid = false;
        }
        else if (date1 === '') {
            $("#date1Error").text("* Please enter a beginning date");
            $("#date1").addClass("errorBorder");
            isValid = false; 
        }
        else if (date1 != '' && dateObject1 >= dateObject2) {
            $("#date1Error").empty();
            $("#date1").removeClass("errorBorder");
            isValid = true;
        }
       
    // if the form validates submit it
     if (isValid) {
        return true;
     }
     else{
         return false;
     }
   });
   
});