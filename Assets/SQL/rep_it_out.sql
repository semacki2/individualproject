-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 13, 2018 at 01:17 AM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 7.0.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rep_it_out`
--

-- --------------------------------------------------------

--
-- Table structure for table `exercise`
--

CREATE TABLE `exercise` (
  `ExerciseID` int(11) NOT NULL,
  `Name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exercise`
--

INSERT INTO `exercise` (`ExerciseID`, `Name`) VALUES
(6, 'Push Up'),
(8, 'Pull Up'),
(9, 'Squat'),
(10, 'Bench Press'),
(11, 'Dead Lift'),
(12, 'Bicep Curl');

-- --------------------------------------------------------

--
-- Table structure for table `session`
--

CREATE TABLE `session` (
  `Date` date NOT NULL,
  `Length` int(11) NOT NULL,
  `SessionID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `session`
--

INSERT INTO `session` (`Date`, `Length`, `SessionID`, `UserID`) VALUES
('2018-03-01', 25, 29, 4),
('2018-03-02', 60, 30, 4),
('2018-03-03', 55, 31, 4),
('2018-03-08', 55, 32, 4),
('2018-02-13', 45, 33, 4),
('2018-03-09', 90, 34, 4),
('2018-02-28', 45, 35, 4),
('2018-03-07', 66, 36, 4),
('2018-03-04', 95, 37, 4),
('2018-03-08', 85, 38, 3),
('2018-03-02', 45, 40, 3),
('2018-03-11', 55, 42, 4),
('2018-03-10', 85, 43, 4),
('2018-03-02', 45, 45, 3),
('2018-03-02', 45, 46, 3),
('2018-03-09', 45, 47, 3),
('2018-03-12', 45, 48, 4),
('2018-03-05', 65, 49, 4),
('2018-03-06', 65, 50, 4),
('2018-03-10', 90, 51, 4),
('2018-03-12', 90, 52, 4);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `UserID` int(11) NOT NULL,
  `Username` varchar(255) NOT NULL,
  `Password` varchar(255) NOT NULL,
  `UserType` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`UserID`, `Username`, `Password`, `UserType`) VALUES
(3, 'admin', '$2y$10$NxKmLWIMiOFHrxVsCazTOuxh8RDgatLyq2.YAAZ9Tam5BCzF0TimG', 'admin'),
(4, 'Seth', '$2y$10$jDNkmcdFouR6dNUBsjuBh.PMYXPxakG.JDngwEV.Os1PLOCfZEKm6', 'user'),
(5, 'guest', '$2y$10$x/c3Sliz36Z6nrupPzfENuFtdgf5qAlNybc0DgrZ0JcZHXXAUYi2W', 'user');

-- --------------------------------------------------------

--
-- Table structure for table `workout`
--

CREATE TABLE `workout` (
  `WorkoutID` int(11) NOT NULL,
  `ExerciseID` int(11) NOT NULL,
  `Sets` int(11) NOT NULL,
  `Reps` int(11) NOT NULL,
  `ResistanceLevel` decimal(10,0) NOT NULL,
  `SessionID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `workout`
--

INSERT INTO `workout` (`WorkoutID`, `ExerciseID`, `Sets`, `Reps`, `ResistanceLevel`, `SessionID`) VALUES
(18, 6, 5, 5, '205', 29),
(19, 8, 5, 5, '205', 29),
(20, 9, 5, 5, '225', 29),
(21, 6, 10, 5, '205', 30),
(22, 8, 8, 8, '205', 30),
(23, 9, 5, 5, '240', 30),
(24, 6, 5, 5, '205', 31),
(25, 8, 10, 10, '205', 31),
(26, 9, 5, 5, '280', 31),
(27, 9, 5, 5, '250', 32),
(28, 11, 8, 8, '375', 33),
(29, 11, 10, 10, '300', 34),
(30, 9, 10, 10, '100', 34),
(31, 10, 10, 15, '125', 34),
(32, 12, 8, 8, '65', 34),
(33, 10, 5, 5, '250', 35),
(34, 11, 10, 1, '350', 35),
(35, 12, 8, 8, '65', 35),
(36, 12, 5, 5, '85', 36),
(37, 6, 7, 3, '205', 37),
(38, 8, 4, 12, '205', 37),
(39, 9, 8, 8, '265', 37),
(40, 10, 5, 8, '235', 37),
(41, 11, 7, 4, '325', 37),
(42, 12, 6, 6, '55', 37),
(43, 8, 5, 5, '25', 38),
(45, 8, 5, 5, '55', 40),
(47, 10, 8, 8, '285', 42),
(48, 9, 5, 8, '265', 43),
(50, 9, 5, 5, '225', 45),
(52, 10, 8, 3, '205', 46),
(53, 9, 8, 5, '65', 47),
(56, 9, 8, 6, '305', 48),
(57, 6, 5, 5, '205', 48),
(58, 12, 8, 8, '35', 33),
(59, 11, 5, 5, '315', 49),
(60, 9, 5, 5, '7', 50),
(61, 10, 5, 5, '225', 36),
(62, 11, 5, 5, '325', 51),
(63, 8, 6, 6, '205', 51),
(64, 11, 5, 5, '295', 52),
(65, 12, 8, 8, '55', 52);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `exercise`
--
ALTER TABLE `exercise`
  ADD PRIMARY KEY (`ExerciseID`);

--
-- Indexes for table `session`
--
ALTER TABLE `session`
  ADD PRIMARY KEY (`SessionID`),
  ADD KEY `UserID` (`UserID`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`UserID`);

--
-- Indexes for table `workout`
--
ALTER TABLE `workout`
  ADD PRIMARY KEY (`WorkoutID`),
  ADD KEY `ExerciseID` (`ExerciseID`),
  ADD KEY `SessionID` (`SessionID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `exercise`
--
ALTER TABLE `exercise`
  MODIFY `ExerciseID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `session`
--
ALTER TABLE `session`
  MODIFY `SessionID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `UserID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `workout`
--
ALTER TABLE `workout`
  MODIFY `WorkoutID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `session`
--
ALTER TABLE `session`
  ADD CONSTRAINT `fkUserID_Session` FOREIGN KEY (`UserID`) REFERENCES `user` (`UserID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `workout`
--
ALTER TABLE `workout`
  ADD CONSTRAINT `fkExerciseID` FOREIGN KEY (`ExerciseID`) REFERENCES `exercise` (`ExerciseID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fkSessionID_Workout` FOREIGN KEY (`SessionID`) REFERENCES `session` (`SessionID`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
