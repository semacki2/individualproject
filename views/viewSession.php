<?php include 'views/header.php'; ?>

<!-- This view shows information for the selected session. -->
<main>

    <div class="card card-body">
        <h4 class="card-title">Session from <?php echo htmlspecialchars($session->getFormattedDate()) ?></h4>

        <p class="card-text">Date: <?php echo htmlspecialchars($session->getFormattedDate()) ?></p>
        <p class="card-text">Length: <?php echo htmlspecialchars($session->getLength()) . " minutes" ?></p>

        <div class="row">
            <div class="col-lg-4">
                <div class="form-group">
                    <p class="card-text">Workouts:</p>

                    <?php if (isset($workouts) && !empty($workouts)) : ?>                            
                        <table class="table table-bordered table-striped">
                            <tr>
                                <th scope="col">Exercise</th>
                                <th scope="col">Sets</th>
                                <th scope="col">Reps</th>
                                <th scope="col">Resistance Level</th>
                                <th scope="col"><a class="btn btn-info btn-sm" href='.?action=addWorkoutToOldSession&sessionID=<?php echo htmlspecialchars($session->getID()) ?>'>Add Workout</a></th>
                                <th scope="col">&nbsp;</th>                                
                            </tr>
                            <?php for ($i = 0; $i < count($workouts); $i++) : ?>
                                <tr>
                                    <td><?php echo htmlspecialchars($workouts[$i]->getExercise()->getName()); ?></td>
                                    <td><?php echo htmlspecialchars($workouts[$i]->getSets()); ?></td>
                                    <td><?php echo htmlspecialchars($workouts[$i]->getReps()); ?></td>
                                    <td><?php echo htmlspecialchars($workouts[$i]->getResistanceLevel()) . " lbs"; ?></td>
                                    <td class="text-center"><a href=".?action=editWorkout&exerciseID=<?php echo htmlspecialchars($workouts[$i]->getExercise()->getID()); ?>&sets=<?php echo htmlspecialchars($workouts[$i]->getSets()); ?>&reps=<?php echo htmlspecialchars($workouts[$i]->getReps()); ?>&resistanceLevel=<?php echo htmlspecialchars($workouts[$i]->getResistanceLevel()); ?>&workoutID=<?php echo htmlspecialchars($workouts[$i]->getID()) ?>&sessionID=<?php echo htmlspecialchars($workouts[$i]->getSessionID()) ?>" class='btn btn-secondary btn-sm' onclick='editWorkout(this);'>Edit Workout</a></td>
                                    <td class="text-center"><a href=".?action=deleteWorkout&workoutID=<?php echo htmlspecialchars($workouts[$i]->getID()) ?>&sessionID=<?php echo htmlspecialchars($workouts[$i]->getSessionID()) ?>" class='btn btn-danger btn-sm' >Delete Workout</a></td>                                    
                                </tr>
                            <?php endfor; ?>                                    
                        </table>
                    <?php else : ?>
                        <br>
                        <a class="btn btn-info btn-sm" href='.?action=addWorkoutToOldSession&sessionID=<?php echo htmlspecialchars($session->getSessionID()) ?>'>Add Workout</a>                                
                        <br>
                        <p class="card-text">No workouts</p>
                    <?php endif; ?>

                </div>
            </div>                    
        </div>

        <div class="row">
            <a href="?action=home" class="btn btn-info">Return</a>                
            <span>&nbsp;&nbsp;&nbsp;</span>
            <form action="." method="post">
                <input type="hidden" name="action" value="deleteSession">
                <input type="hidden" name="sessionID" value="<?php echo htmlspecialchars($session->getID()); ?>">
                <input type="submit" value="Delete Session" class="btn btn-danger">
            </form>
        </div>
    </div>
</main>
<?php include 'views/footer.php'; ?>