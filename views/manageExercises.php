<?php include 'views/header.php'; ?>

<!-- This view allows an admin to add/edit/delete exercises. -->
<main>

    <div class="card card-body">
        <h4 class="card-title">Exercises</h4>
        <div class="row">
            <div class="col-lg-6">

                <?php if (isset($exercises) && !empty($exercises)) : ?>                            
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th scope="col">Exercise</th>
                            <th scope="col" class="text-center"><a href="?action=addExercise" class="btn btn-primary">Add New Exercise</a></th>
                            <th scope="col">&nbsp;</th>
                        </tr>

                        <?php foreach ($exercises as $e) : ?>
                            <tr>
                                <td><?php echo htmlspecialchars($e->getName()); ?></td>
                                <td class="text-center">
                                    <form action="." method="post">
                                        <input type="hidden" name="action" value="editExercise">
                                        <input type="hidden" name="exerciseID" value="<?php echo htmlspecialchars($e->getID()); ?>">
                                        <input type="submit" value="Edit Exercise" class="btn btn-info">
                                    </form>
                                </td>
                                <td class="text-center">
                                    <form action="." method="post">
                                        <input type="hidden" name="action" value="deleteExercise">
                                        <input type="hidden" name="exerciseID" value="<?php echo htmlspecialchars($e->getID()); ?>">
                                        <input type="submit" value="Delete Exercise" class="btn btn-danger">
                                    </form>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </table>        
                <?php else : ?>
                    <a href="?action=addSessionForm" class="btn btn-primary">Add New Exercise</a>
                    <p>No exercises</p>
                <?php endif; ?>
            </div>
        </div>
    </div>
</main>
<?php include 'views/footer.php'; ?>