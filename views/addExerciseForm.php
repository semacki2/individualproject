<?php include 'views/header.php'; ?>

<!-- This view has the form to create an exercise. -->

<main>

    <div class="card card-body">
        <h4 class="card-title">New Exercise</h4>

        <div class="container">
            <!-- Display error messages -->
            <p class="error"><?php
                if (!empty($errorMessages)) {
                    foreach ($errorMessages as $error) {
                        if ($error !== FALSE && $error !== '') {
                            echo htmlspecialchars($error);
                            echo '<br>';
                        }
                    }
                }
                ?></p>

            <form action="." method="post" id="createExerciseForm">

                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="sets">Exercise</label>
                            <input class="form-control" id="exerciseName" name="exerciseName" placeholder="Exercise Name" type="text" 
                                   value="<?php
                                   if (isset($exerciseName)) {
                                       echo htmlspecialchars($exerciseName);
                                   }
                                   ?>">
                        </div>
                    </div>                
                </div>

                <input type="hidden" name="action" value="validateAddExercise">                
                <button type="submit" class="btn btn-primary">Create Exercise</button>
                <a href="?action=manageExercises" class="card-link"><button type="button" class="btn btn-danger">Cancel</button></a>

            </form>
        </div>
    </div>
</main>
<?php include 'views/footer.php'; ?>
