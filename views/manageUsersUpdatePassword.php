<?php include 'views/header.php'; ?>

<!-- This view allows an admin to change another user's password. -->
<main> 
    <div class="card card-body">
        <h4 class="card-title">Manage Users - Update Password</h4>        
        <div class="container">
            <form action="." method="post">

                <!-- Display error messages -->
                <p class="error"><?php
                    if (!empty($errorMessages)) {
                        foreach ($errorMessages as $error) {
                            if ($error !== FALSE && $error !== '') {
                                echo htmlspecialchars($error);
                                echo '<br>';
                            }
                        }
                    }
                    ?></p>             

                <input type="hidden" name="action" value="validateManageUsersUpdatePassword">

                <div class="form-group row">
                    <label for="selectUser" class="col-2 col-form-label">User:</label>

                    <!-- Select box that holds all the users. -->
                    <div class="col-lg-6">
                        <select id="selectUser" name="selectUser" class="form-control">
                            <option value='' <?php if (isset($userID) && $userID == '') echo ' selected = "selected"' ?>>User</option>
                            <?php
                            foreach ($allUsers as $user) {
                                if (isset($userID) && $user->getID() == $userID) {
                                    echo "<option value=" . htmlspecialchars($user->getID()) . " selected = 'selected'>" . htmlspecialchars($user->getUsername()) . "</option>";
                                } else {
                                    echo "<option value=" . htmlspecialchars($user->getID()) . ">" . htmlspecialchars($user->getUsername()) . "</option>";
                                }
                            }
                            ?>
                        </select>   
                    </div>

                </div>

                <div class="form-group row">
                    <label class="col-2 col-form-label">Password:</label>
                    <div class="col-lg-6">
                        <input type="password" class="form-control" placeholder="Password" name="password" value=<?php if (isset($password)) echo htmlspecialchars($password) ?>><br>
                    </div>                
                </div>

                <div class="form-group row">
                    <div class="col-lg-6">
                        <div class="row">
                            <button type="submit" class="btn btn-primary">Update Password</button>
                            <span>&nbsp;&nbsp;&nbsp;</span>
                            <a href="?action=home" class="btn btn-danger">Cancel</a>
                        </div>
                    </div>
                </div>
                
            </form>
        </div>
    </div>
</main>
<?php include 'views/footer.php'; ?>