
<?php include 'views/header.php'; ?>

<!-- This view is the form to add a new session. -->

<main>

    <div class="card card-body">
        <h4 class="card-title">Add Session</h4>
        <div class="container">
            <form action="." method="post" id="addSessionForm" name="addSessionForm">
                <!-- Display error messages -->
                <p class="error"><?php
                    if (!empty($errorMessages)) {
                        foreach ($errorMessages as $error) {
                            if ($error !== FALSE && $error !== '') {
                                echo htmlspecialchars($error);
                                echo '<br>';
                            }
                        }
                    }
                    ?></p>

                <input type="hidden" name="action" id="action" value="validateAddSession">

                <div class="row">
                    <div class='col-lg-6'>
                        <div class="form-group">
                            <label for="date">Date</label>
                            <input class="form-control" id="date" name="date" placeholder="date" type="text" 
                                   value="<?php
                                   if (isset($date)) {
                                       echo htmlspecialchars($date);
                                   }
                                   ?>">
                        </div>
                    </div>                            
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="length">Length (in minutes)</label>
                            <input class="form-control" id="length" name="length" placeholder="length" type="text" 
                                   value="<?php
                                   if (isset($length)) {
                                       echo htmlspecialchars($length);
                                   }
                                   ?>">
                        </div>

                    </div>                
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="length">Workouts</label> 

                            <!-- Table to show workouts attached to this session -->
                            <?php if (isset($workouts) && !empty($workouts)) : ?>                            
                                <table class="table table-bordered table-striped">
                                    <tr>
                                        <th scope="col">Exercise</th>
                                        <th scope="col">Sets</th>
                                        <th scope="col">Reps</th>
                                        <th scope="col">Resistance Level</th>
                                        <th scope="col"><button type="button" class="btn btn-info btn-sm" id="btnAddWorkoutToSession" name="btnAddWorkoutToSession">Add Workout</button></th>
                                        <th scope="col">&nbsp;</th>
                                    </tr>
                                    <?php for ($i = 0; $i < count($workouts); $i++) : ?>
                                        <tr>
                                            <td><?php echo htmlspecialchars($workouts[$i]->getExercise()->getName()); ?></td>
                                            <td><?php echo htmlspecialchars($workouts[$i]->getSets()); ?></td>
                                            <td><?php echo htmlspecialchars($workouts[$i]->getReps()); ?></td>
                                            <td><?php echo htmlspecialchars($workouts[$i]->getResistanceLevel()) . " lbs"; ?></td>
                                            <td class="text-center"><a href=".?action=editWorkout&exerciseID=<?php echo htmlspecialchars($workouts[$i]->getExercise()->getID()); ?>&sets=<?php echo htmlspecialchars($workouts[$i]->getSets()); ?>&reps=<?php echo htmlspecialchars($workouts[$i]->getReps()); ?>&resistanceLevel=<?php echo htmlspecialchars($workouts[$i]->getResistanceLevel()); ?>&index=<?php echo htmlspecialchars($i) ?>" class='btn btn-secondary btn-sm' onclick='editWorkout(this);'>Edit Workout</a></td>
                                            <td class="text-center"><a href=".?action=removeWorkout&exerciseID=<?php echo htmlspecialchars($workouts[$i]->getExercise()->getID()); ?>&sets=<?php echo htmlspecialchars($workouts[$i]->getSets()); ?>&reps=<?php echo htmlspecialchars($workouts[$i]->getReps()); ?>&resistanceLevel=<?php echo htmlspecialchars($workouts[$i]->getResistanceLevel()); ?>&index=<?php echo htmlspecialchars($i) ?>" class='btn btn-danger btn-sm' onclick='editWorkout(this);'>Remove Workout</a></td>
                                        </tr>
                                    <?php endfor; ?>                                    
                                </table>
                            <?php else : ?>
                                <br>
                                <button type="button" class="btn btn-info btn-sm" id="btnAddWorkoutToSession" name="btnAddWorkoutToSession">Add Workout</button>                                
                                <br>
                                <p>No workouts</p>
                            <?php endif; ?>

                        </div>
                    </div>                    
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <span>
                            <button type="submit" class="btn btn-primary">Add Session</button>
                            <a href="?action=cancelAddSession" class="card-link"><button type="button" class="btn btn-danger">Cancel</button></a>
                        </span>
                    </div>
                </div>

            </form>
        </div>
    </div>
</main>
<?php include 'views/footer.php'; ?>