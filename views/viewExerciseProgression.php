<?php include 'views/header.php'; ?>

<!-- This view generates a graph that shows the users progression for a given exercise. -->
<main>

    <div class="card card-body">
        <h4 class="card-title">Exercise Progression</h4>
        
        <div class="row">

            <!-- display error messages -->
            <p class="error"><?php
                if (!empty($errorMessages)) {
                    foreach ($errorMessages as $error) {
                        if ($error !== FALSE && $error !== '') {
                            echo htmlspecialchars($error);
                            echo '<br>';
                        }
                    }
                }
                ?></p>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <!-- This container is where the fusion chart will render -->
                <div id="chart-1"></div>
            </div>
        </div>

        <form action="." method="post" id="generateExerciseChart">
            <div class="row">               
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="exercise">Exercise</label>

                        <select id="exercise" name="exercise" class="form-control">
                            <option value='' <?php if (isset($exerciseID) && $exerciseID == '') echo ' selected = "selected"' ?>>Exercise</option>
                            <?php
                            foreach ($allExercises as $exercise) {
                                if (isset($exerciseID) && $exercise->getID() == $exerciseID) {
                                    echo "<option value=" . htmlspecialchars($exercise->getID()) . " selected = 'selected'>" . htmlspecialchars($exercise->getName()) . "</option>";
                                } else {
                                    echo "<option value=" . htmlspecialchars($exercise->getID()) . ">" . htmlspecialchars($exercise->getName()) . "</option>";
                                }
                            }
                            ?>
                        </select>   
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <input type="hidden" name="action" value="viewExerciseProgression">
                    <button type="submit" class="btn btn-primary">Generate Report</button>
                    <a href="?action=home" class="card-link"><button type="button" class="btn btn-danger">Cancel</button></a>
                </div>                    
            </div>
        </form>      
    </div>
</main>
<?php include 'views/footer.php'; ?>