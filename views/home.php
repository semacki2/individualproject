<?php include 'views/header.php'; ?>

<!-- The home page view that displays the users workout sessions in a table. -->
<main>

    <div class="card card-body">
        <h4 class="card-title">Workout Sessions</h4>
        <div class="row">
            <div class="col-lg-6">

                <!-- Display error messages -->
                <p class="error"><?php
                    if (!empty($errorMessages)) {
                        foreach ($errorMessages as $error) {
                            if ($error !== FALSE && $error !== '') {
                                echo htmlspecialchars($error);
                                echo '<br>';
                            }
                        }
                    }
                    ?></p>

                <?php if (isset($sessions) && !empty($sessions)) : ?>                            
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th scope="col">Date</th>
                            <th scope="col">Length</th>
                            <th scope="col" class="text-center"><a href="?action=addSessionForm" class="btn btn-primary">Add New Session</a></th>
                            <th scope="col">&nbsp;</th>
                        </tr>

                        <?php foreach ($sessions as $s) : ?>
                            <tr>
                                <td><?php echo htmlspecialchars($s->getFormattedDate()); ?></td>
                                <td><?php echo htmlspecialchars($s->getLength() . " minutes"); ?></td>
                                <td class="text-center">
                                    <form action="." method="post">
                                        <input type="hidden" name="action" value="viewSession">
                                        <input type="hidden" name="sessionID" value="<?php echo htmlspecialchars($s->getID()); ?>">
                                        <input type="submit" value="View Session" class="btn btn-info">
                                    </form>
                                </td>
                                <td class="text-center">
                                    <form action="." method="post">
                                        <input type="hidden" name="action" value="deleteSession">
                                        <input type="hidden" name="sessionID" value="<?php echo htmlspecialchars($s->getID()); ?>">
                                        <input type="submit" value="Delete Session" class="btn btn-danger">
                                    </form>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </table>        
                <?php else : ?>
                    <a href="?action=addSessionForm" class="btn btn-primary">Add New Session</a>
                    <p>No sessions</p>
                <?php endif; ?>

            </div>
        </div>
    </div>
</main>
<?php include 'views/footer.php'; ?>