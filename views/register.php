<?php include 'views/header.php'; ?>

<!-- This view allows new users to register an account. -->
<main> 
    <div class="card card-body">
        <h4 class="card-title">Register New Account</h4>
        <div class="container">
            <form action="." method="post">

                <!-- Display error messages -->
                <p class="error"><?php
                    if (!empty($errorMessages)) {
                        foreach ($errorMessages as $error) {
                            if ($error !== FALSE && $error !== '') {
                                echo htmlspecialchars($error);
                                echo '<br>';
                            }
                        }
                    }
                    ?></p>

                <input type="hidden" name="action" value="validateRegister">

                <div class="form-group row">
                    <label class="col-2 col-form-label">Username:</label>
                    <div class="col-lg-6">
                        <input type="text" class="form-control" placeholder="Username" name="username" value=<?php if (isset($username)) echo htmlspecialchars($username) ?>><br>
                    </div>                
                </div>

                <div class="form-group row">
                    <label class="col-2 col-form-label">Password:</label>
                    <div class="col-lg-6">
                        <input type="password" class="form-control" placeholder="Password" name="password" value=<?php if (isset($password)) echo htmlspecialchars($password) ?>><br>
                    </div>                
                </div>

                <div class="form-group row">
                    <div class="col-lg-6">
                        <div class="row">
                            <button type="submit" class="btn btn-primary">Register</button>
                            <span>&nbsp;&nbsp;&nbsp;</span>
                            <a href="?action=login" class="btn btn-info">Login</a>
                        </div>                    
                    </div>
                </div>
                
            </form>
        </div>
    </div>
</main>
<?php include 'views/footer.php'; ?>