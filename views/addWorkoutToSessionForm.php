<?php include 'views/header.php'; ?>

<!-- This view has the form to add a workout to a session. -->

<main>

    <div class="card card-body">
        <h4 class="card-title">New Workout</h4>
        <div class="container">
            <!-- Display error messages -->
            <p class="error"><?php
                if (!empty($errorMessages)) {
                    foreach ($errorMessages as $error) {
                        if ($error !== FALSE && $error !== '') {
                            echo htmlspecialchars($error);
                            echo '<br>';
                        }
                    }
                }
                ?></p>

            <form action="." method="post" id="addWorkoutToSessionForm">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="exercise">Exercise</label>

                            <!-- Select box that holds all the exercises. -->
                            <select id="exercise" name="exercise" class="form-control">
                                <option value='' <?php if (isset($exerciseID) && $exerciseID == '') echo ' selected = "selected"' ?>>Exercise</option>
                                <?php
                                foreach ($allExercises as $exercise) {
                                    if (isset($exerciseID) && $exercise->getID() == $exerciseID) {
                                        echo "<option value=" . htmlspecialchars($exercise->getID()) . " selected = 'selected'>" . htmlspecialchars($exercise->getName()) . "</option>";
                                    } else {
                                        echo "<option value=" . htmlspecialchars($exercise->getID()) . ">" . htmlspecialchars($exercise->getName()) . "</option>";
                                    }
                                }
                                ?>
                            </select>   
                        </div>
                    </div>                
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="sets">Sets</label>
                            <input class="form-control" id="sets" name="sets" placeholder="Sets" type="text" 
                                   value="<?php
                                   if (isset($sets)) {
                                       echo htmlspecialchars($sets);
                                   }
                                   ?>">
                        </div>
                    </div>                
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="reps">Reps</label>
                            <input class="form-control" id="reps" name="reps" placeholder="Reps" type="text" 
                                   value="<?php
                                   if (isset($reps)) {
                                       echo htmlspecialchars($reps);
                                   }
                                   ?>">
                        </div>
                    </div>                
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="resistanceLevel">Resistance Level (in lbs)</label>
                            <input class="form-control" id="resistanceLevel" name="resistanceLevel" placeholder="Resistance Level" type="text" 
                                   value="<?php
                                   if (isset($resistanceLevel)) {
                                       echo htmlspecialchars($resistanceLevel);
                                   }
                                   ?>">
                        </div>
                    </div>                
                </div>

                <input type="hidden" name="action" value="validateAddWorkoutToSession">
                <input type="hidden" name="sessionID" value="<?php if(isset($sessionID)) echo htmlspecialchars($sessionID) ?>">
                <button type="submit" class="btn btn-primary">Add Workout</button>
                <a href="?action=cancelAddWorkout&sessionID=<?php if(isset($sessionID)) echo htmlspecialchars($sessionID) ?>" class="card-link"><button type="button" class="btn btn-danger">Cancel</button></a>

            </form>      
        </div>
    </div>
</main>
<?php include 'views/footer.php'; ?>
