<?php include 'views/header.php'; ?>

<!-- This view allows an admin to create a new user account. -->
<main> 
    <div class="card card-body">
        <h4 class="card-title">Manage Users - Create User</h4>        
        <div class="container">
            <form action="." method="post">

                <!-- Display error messages -->
                <p class="error"><?php
                    if (!empty($errorMessages)) {
                        foreach ($errorMessages as $error) {
                            if ($error !== FALSE && $error !== '') {
                                echo htmlspecialchars($error);
                                echo '<br>';
                            }
                        }
                    }
                    ?></p>             

                <input type="hidden" name="action" value="validateManageUsersCreateUser">

                <div class="form-group row">
                    <label class="col-2 col-form-label">Username:</label>
                    <div class="col-lg-6">
                        <input type="text" class="form-control" placeholder="Username" name="username" value=<?php if (isset($username)) echo htmlspecialchars($username) ?>><br>
                    </div>                
                </div>

                <div class="form-group row">
                    <label class="col-2 col-form-label">Password:</label>
                    <div class="col-lg-6">
                        <input type="password" class="form-control" placeholder="Password" name="password" value=<?php if (isset($password)) echo htmlspecialchars($password) ?>><br>
                    </div>                
                </div>

                <div class="form-group row">
                    <label class="col-2 col-form-label">User Type:</label>
                    <div class="col-lg-6">

                        <select class="form-control" name="userType">
                            <option value="user" selected>Standard User</option>  
                            <option value="admin">Admin</option>                                                      
                        </select>                        
                    </div>                
                </div>

                <div class="form-group row">
                    <div class="col-lg-6">
                        <div class="row">
                            <button type="submit" class="btn btn-primary">Create User</button>
                            <span>&nbsp;&nbsp;&nbsp;</span>
                            <a href="?action=home" class="btn btn-danger">Cancel</a>
                        </div>
                    </div>
                </div>
                
            </form>
        </div>        
    </div>
</main>
<?php include 'views/footer.php'; ?>