
<!DOCTYPE html>
<html>

    <!-- the head section -->
    <head>
        <title>Rep It Out</title>
        <!-- Load CSS -->    
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css"  href="Assets/css/style.css">
        <link rel="stylesheet" type="text/css"  href="Assets/css/jquery-ui.css">
    </head>


    <body>

        <!-- The navigation bar -->
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">

            <a class="navbar-brand" href=".?action=home">Rep It Out!</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarText">                
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href=".?action=home">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href=".?action=addSessionForm">Add New Session</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href=".?action=viewExerciseProgression">Exercise Progression</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href=".?action=manageUsersUpdatePassword"><?php if (isset($_SESSION['activeUser']) && $_SESSION['activeUser']->getUserType() == 'admin') echo "Manage User Passwords" ?></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href=".?action=manageUsersCreateUser"><?php if (isset($_SESSION['activeUser']) && $_SESSION['activeUser']->getUserType() == 'admin') echo "Create New User" ?></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href=".?action=manageUsersDeleteUser"><?php if (isset($_SESSION['activeUser']) && $_SESSION['activeUser']->getUserType() == 'admin') echo "Delete User" ?></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href=".?action=manageExercises"><?php if (isset($_SESSION['activeUser']) && $_SESSION['activeUser']->getUserType() == 'admin') echo "Manage Exercises" ?></a>
                    </li>
                </ul>

                <ul class="navbar-nav navbar-right">

                    <li class="nav-item dropdown">
                        <?php if (isset($_SESSION['activeUsername'])) echo "<a class='nav-link dropdown-toggle' href='#' data-toggle='dropdown'><i class='far fa-user'></i> " . htmlspecialchars($_SESSION['activeUsername']) . "</a>" ?>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href=".?action=updatePassword">Update Password</a>
                        </div>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href=".?action=login"><?php if (!isset($_SESSION['activeUsername'])) echo "<i class='fas fa-sign-in-alt'></i> Login" ?></a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href=".?action=login"><?php if (isset($_SESSION['activeUsername'])) echo "<i class='fas fa-sign-out-alt'></i> Logout" ?></a>
                    </li>
                </ul>
            </div>
        </nav>
        <div class="container-fluid">            


