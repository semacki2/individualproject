<?php include 'views/header.php'; ?>
<main>
    <div class="card card-body">
        <h4 class="card-title">Database Error</h4>
        <p class="card-text">There was an error connecting to the database.</p>
        <p class="card-text">Error message: <?php echo htmlspecialchars($error_message); ?></p>
    </div>
</main>
<?php include 'views/footer.php'; ?>