<?php include 'views/header.php'; ?>
<main>
    <div class="card card-body">
        <h4 class="card-title">Data Type Error</h4>
        <p class="card-text">An invalid data type was used.</p>
        <p class="card-text">Error message: <?php echo htmlspecialchars($error_message); ?></p>
    </div>
</main>
<?php include 'views/footer.php'; ?>