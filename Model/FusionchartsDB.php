<?php
/*
 * Database class that creates FusionChart object based on data from the database.
 */
class FusionchartsDB {

    public static function generateExerciseChartByUserIDAndExerciseID($userID, $exerciseID) {
        $db = Database::getDB();
        $query = 'SELECT w.ResistanceLevel, DATE_FORMAT(s.Date, "%b %e, %Y") AS "Date", (w.Sets * w.Reps) AS "Total Reps" '
                . 'FROM workout AS w JOIN session AS s ON w.SessionID = s.SessionID JOIN exercise AS e ON w.ExerciseID = e.ExerciseID '
                . 'WHERE s.UserID = :userID AND w.ExerciseID = :exerciseID ORDER BY s.Date';
        $statement = $db->prepare($query);
        $statement->bindValue(':exerciseID', $exerciseID);
        $statement->bindValue(':userID', $userID);
        $statement->execute();
        $rows = $statement->fetchAll();

        $statement->closeCursor();

        $arrData = array(
            "chart" => array(
                "caption" => ExerciseDB::getExerciseByID($exerciseID)->getName(),
                "subCaption" => "Workout Progression",
                "captionFontSize" => "14",
                "subcaptionFontSize" => "14",
                "subcaptionFontBold" => "0",
                "paletteColors" => "#0075c2,#1aaf5d",
                "bgcolor" => "#ffffff",
                "showBorder" => "0",
                "showShadow" => "1",
                "showCanvasBorder" => "0",
                "usePlotGradientColor" => "0",
                "legendBorderAlpha" => "0",
                "legendShadow" => "0",
                "showAxisLines" => "0",
                "showAlternateHGridColor" => "0",
                "divlineThickness" => "1",
                "divLineIsDashed" => "1",
                "divLineDashLen" => "1",
                "divLineGapLen" => "1",
                "xAxisName" => "Day",
                "showValues" => "1"
            )
        );

        $categoryArray = array();
        $dataseries1 = array();
        $dataseries2 = array();

        if (!empty($rows)) {
            // Push the data into the array
            foreach ($rows as $row) {
                array_push($categoryArray, array(
                    "label" => $row["Date"]
                        )
                );
            }
        }

        if (!empty($rows)) {
            // Push the data into the array
            foreach ($rows as $row) {
                array_push($dataseries1, array(
                    "value" => $row["ResistanceLevel"]
                        )
                );
            }
        }

        if (!empty($rows)) {
            // Push the data into the array
            foreach ($rows as $row) {
                array_push($dataseries2, array(
                    "value" => $row["Total Reps"]
                        )
                );
            }
        }

        $arrData["categories"] = array(array("category" => $categoryArray));

        // creating dataset object
        $arrData["dataset"] = array(array("seriesName" => "Resistance Level (in lbs)", "data" => $dataseries1), array("seriesName" => "Total Reps", "renderAs" => "line", "data" => $dataseries2));


        /* JSON Encode the data to retrieve the string containing the JSON representation of the data in the array. */

        $jsonEncodedData = json_encode($arrData);

        /* Create an object for the column chart using the FusionCharts PHP class constructor. 
         * Syntax for the constructor is ` FusionCharts("type of chart", "unique chart id", width of the chart, height of the chart, "div id to render the chart", "data format", "data source")`. 
         * Because we are using JSON data to render the chart, the data format will be `json`. 
         * The variable `$jsonEncodeData` holds all the JSON data for the chart, and will be passed as the value for the data source parameter of the constructor. 
         * 
         * Please note:
         * This comment was taken from the FushionCharts documentation
         */

        $lineChart = new FusionCharts("msline", "exerciseChart", 1000, 500, "chart-1", "json", $jsonEncodedData);

        // Render the chart
        $lineChart->render();
    }

}
