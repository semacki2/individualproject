<?php

/*
 * Utility class that validates data.
 */

class Validate {

    public function __construct() {
        
    }

    //Accepts a string array and returns true if the array contains an error and false if the array is empty.
    public static function errorIsPresent($errorMessages) {
        $dataIsValid = true;
        if (!empty($errorMessages)) {
            foreach ($errorMessages as $error) {
                if (isset($error) && $error !== FALSE) {
                    $dataIsValid = false;
                }
            }
        }
        return !$dataIsValid;
    }

    //Accepts a string username and queries the database for that username. If the username exists in the database, return true; else, return false.
    public static function usernameExists($username) {
        $db = Database::getDB();

        $query = 'SELECT username FROM user WHERE username = :username';
        $statement = $db->prepare($query);
        $statement->bindValue(':username', $username);
        $statement->execute();
        $results = $statement->fetch();
        $statement->closeCursor();

        if (!$results || $results === NULL) {
            return false;
        } else {
            return true;
        }
    }

    //Accepts a username and password and determines if the entered password matches the user's hashed password that is stored in the database.
    public static function passwordIsCorrect($username, $password) {
        $user = UserDB::getUserByUsername($username);
        $hashedPassword = $user->getPassword();

        if (password_verify($password, $hashedPassword)) {
            return true;
        } else {
            return false;
        }
    }

    //Determines if the provided variable is a properly formatted date. Must be in YYYY-MM-DD format.
    public static function isDate($date) {
        $subject = $date;
        $pattern = "/^\d{4}\-(0?[1-9]|1[012])\-(0?[1-9]|[12][0-9]|3[01])$/";

        if (preg_match($pattern, $subject)) {
            return true;
        } else {
            return false;
        }
    }

    //Determines if a given date is after today
    public static function isFutureDate($date) {
        $date = strtotime($date);
        $today = strtotime('now');

        if ($date > $today) {
            return true;
        } else {
            return false;
        }
    }

    //Determines if the provided variable is a positive integer.
    public static function isInteger($integer) {
        $subject = $integer;
        $pattern = "/^\d+$/";

        if (preg_match($pattern, $subject) && $integer >= 0) {
            return true;
        } else {
            return false;
        }
    }

    //Determines if the provided variable is a positive decimal. 
    public static function isDecimal($decimal) {
        $subject = $decimal;
        $pattern = "/^(\d*\.)?\d+$/";

        if (preg_match($pattern, $subject) && $decimal >= 0) {
            return true;
        } else {
            return false;
        }
    }

}

?>