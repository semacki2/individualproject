<?php
/*
 * Business class that creates an Exercise object
 */
class Exercise {

    private $ID, $Name;

    function __construct($Name) {
        $this->Name = $Name;
    }

    public static function create() {
        $instance = new self(null);
        return $instance;
    }

    function getID() {
        return $this->ID;
    }

    function getName() {
        return $this->Name;
    }
    
    function setID($ID) {
        $this->ID = $ID;
    }

    function setName($Name) {
        $this->Name = $Name;
    }
}
