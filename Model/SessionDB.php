<?php

/*
 * Database class that creates Session objects from records in the database, deletes Session records from the database, and adds Session records to the database.
 */

class SessionDB {

    //Returns an array of all Session objects in the database
    public static function getSessions() {
        $db = Database::getDB();
        $query = 'SELECT * FROM session ORDER BY sessionID';
        $statement = $db->prepare($query);
        $statement->execute();
        $rows = $statement->fetchAll();
        $statement->closeCursor();
        $sessions = array();
        foreach ($rows as $row) {
            $s = Session::create();
            $s->setID($row['SessionID']);
            $workouts = WorkoutDB::getWorkoutsBySessionID($s->getID());
            $s->setDate($row['Date']);
            $s->setLength($row['Length']);
            $s->setUser(UserDB::getUserByID($row['UserID']));
            $s->setWorkouts($workouts);
            $sessions[] = $s;
        }
        return $sessions;
    }

    //Returns an array of Session objects for the provided user ID
    public static function getSessionsByUserID($userID) {
        $db = Database::getDB();
        $query = 'SELECT * FROM session WHERE userID = :userID ORDER BY date DESC, sessionID DESC';
        $statement = $db->prepare($query);
        $statement->bindValue(':userID', $userID);
        $statement->execute();
        $rows = $statement->fetchAll();
        $statement->closeCursor();
        $sessions = array();
        foreach ($rows as $row) {
            $s = Session::create();
            $s->setID($row['SessionID']);
            $workouts = WorkoutDB::getWorkoutsBySessionID($s->getID());
            $s->setDate($row['Date']);
            $s->setLength($row['Length']);
            $s->setUser(UserDB::getUserByID($row['UserID']));
            $s->setWorkouts($workouts);
            $sessions[] = $s;
        }
        return $sessions;
    }

    //Returns a Session object for the provided session ID
    public static function getSessionByID($sessionID) {
        $db = Database::getDB();
        $query = 'SELECT * FROM session WHERE sessionID = :sessionID';
        $statement = $db->prepare($query);
        $statement->bindValue(':sessionID', $sessionID);
        $statement->execute();
        $row = $statement->fetch();
        $statement->closeCursor();

        $s = Session::create();
        $s->setID($row['SessionID']);
        $workouts = WorkoutDB::getWorkoutsBySessionID($s->getID());
        $s->setDate($row['Date']);
        $s->setLength($row['Length']);
        $s->setUser(UserDB::getUserByID($row['UserID']));
        $s->setWorkouts($workouts);
        
        return $s;
    }

    //Deletes the provided Session from the database
    public static function deleteSession($sessionID) {
        $db = Database::getDB();

        $query = 'DELETE FROM session WHERE sessionID = :sessionID';
        $statement = $db->prepare($query);
        $statement->bindValue(':sessionID', $sessionID);
        $statement->execute();
        $statement->closeCursor();
    }

    //Adds the provided Session to the database and returns the newly created Session's ID
    public static function addSession($session) {
        $db = Database::getDB();

        $query = 'INSERT INTO session (Date, Length, UserID)
                  VALUES (:date, :length, :userID)';
        $statement = $db->prepare($query);
        $statement->bindValue(':date', $session->getDate());
        $statement->bindValue(':length', $session->getLength());
        $statement->bindValue(':userID', $session->getUser()->getID());
        $statement->execute();
        $sessionID = $db->lastInsertID();
        $statement->closeCursor();
        return $sessionID;
    }

}
