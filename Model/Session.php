<?php

/*
 * Business class that creates a Session object
 */

class Session {

    private $ID, $Date, $Length, $User, $Workouts;

    function __construct($Date, $Length, $User, $Workouts) {
        $this->Date = $Date;
        $this->Length = $Length;
        $this->User = $User;
        $this->Workouts = $Workouts;
    }

    public static function create() {
        $instance = new self(null,null,null,null);
        return $instance;
    }

    function getID() {
        return $this->ID;
    }

    function getDate() {
        return $this->Date;
    }

    function getLength() {
        return $this->Length;
    }

    function getUser() {
        return $this->User;
    }

    function getWorkouts() {
        return $this->Workouts;
    }

    function setID($ID) {
        $this->ID = $ID;
    }

    function setDate($Date) {
        $this->Date = $Date;
    }

    function setLength($Length) {
        $this->Length = $Length;
    }

    function setUser($User) {
        $this->User = $User;
    }

    function setWorkouts($Workouts) {
        $this->Workouts = $Workouts;
    }
    
    //Adds a new workout to the Session
    function addWorkoutToSession($Workout){
        $this->Workouts[] = $Workout;
    }
    
    //Returns the session's date in a pretty format
    function getFormattedDate(){
        $date = date_create($this->getDate());
        $formattedDate = date_format($date, "M d, Y");
        return $formattedDate;
    }

}
