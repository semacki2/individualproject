<?php

/*
 * Business class that creates a Workout object
 */

class Workout {

    private $ID, $Exercise, $Sets, $Reps, $ResistanceLevel, $SessionID;

    function __construct($Exercise, $Sets, $Reps, $ResistanceLevel, $SessionID) {
        $this->Exercise = $Exercise;
        $this->Sets = $Sets;
        $this->Reps = $Reps;
        $this->ResistanceLevel = $ResistanceLevel;
        $this->SessionID = $SessionID;
    }

    public static function create() {
        $instance = new self(null, null, null, null, null);
        return $instance;
    }

    function getID() {
        return $this->ID;
    }

    function getExercise() {
        return $this->Exercise;
    }

    function getSets() {
        return $this->Sets;
    }

    function getReps() {
        return $this->Reps;
    }

    function getResistanceLevel() {
        return $this->ResistanceLevel;
    }

    function setID($ID) {
        $this->ID = $ID;
    }

    function setExercise($Exercise) {
        $this->Exercise = $Exercise;
    }

    function setSets($Sets) {
        $this->Sets = $Sets;
    }

    function setReps($Reps) {
        $this->Reps = $Reps;
    }

    function setResistanceLevel($ResistanceLevel) {
        $this->ResistanceLevel = $ResistanceLevel;
    }
    
    function getSessionID() {
        return $this->SessionID;
    }

    function setSessionID($SessionID) {
        $this->SessionID = $SessionID;
    }

}
