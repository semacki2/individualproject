<?php

/*
 * Business class that creates a User object.
 */

class User {

    private $ID, $Username, $Password, $UserType;

    function __construct($Username, $Password, $UserType) {
        $this->Username = $Username;
        $this->Password = $Password;
        $this->UserType = $UserType;
    }

    public static function create() {
        $instance = new self(null, null, null);
        return $instance;
    }

    function getID() {
        return $this->ID;
    }

    function getUsername() {
        return $this->Username;
    }

    function getPassword() {
        return $this->Password;
    }

    function getUserType() {
        return $this->UserType;
    }

    function setID($ID) {
        $this->ID = $ID;
    }

    function setUsername($Username) {
        $this->Username = $Username;
    }

    function setPassword($Password) {
        $this->Password = $Password;
    }

    function setUserType($UserType) {
        $this->UserType = $UserType;
    }

}
