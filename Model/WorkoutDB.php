<?php

/*
 * Database class that creates Workout objects from records in the database, deletes Workout records from the database, and adds Workout records to the database.
 */

class WorkoutDB {

    //Returns an array of all Workout objects in the database
    public static function getWorkouts() {
        $db = Database::getDB();
        $query = 'SELECT * FROM workouts ORDER BY workoutID';
        $statement = $db->prepare($query);
        $statement->execute();
        $rows = $statement->fetchAll();
        $statement->closeCursor();
        $workouts = array();
        foreach ($rows as $row) {
            $w = Workout::create();
            $w->setID($row['WorkoutID']);
            $w->setExercise(ExerciseDB::getExerciseByID($row['ExerciseID']));
            $w->setReps($row['Reps']);
            $w->setResistanceLevel($row['ResistanceLevel']);
            $w->setSets($row['Sets']);
            $w->setSessionID($row['SessionID']);
            $workouts[] = $w;
        }
        return $workouts;
    }

    //Returns a Workout object based on a given workout ID
    public static function getWorkoutByID($workoutID) {
        $db = Database::getDB();
        $query = 'SELECT * FROM workout WHERE workoutID = :workoutID';
        $statement = $db->prepare($query);
        $statement->bindValue(':workoutID', $workoutID);
        $statement->execute();
        $row = $statement->fetch();
        $statement->closeCursor();

        $w = Workout::create();
        $w->setID($row['WorkoutID']);
        $w->setExercise(ExerciseDB::getExerciseByID($row['ExerciseID']));
        $w->setReps($row['Reps']);
        $w->setResistanceLevel($row['ResistanceLevel']);
        $w->setSets($row['Sets']);
        $w->setSessionID($row['SessionID']);
        return $w;
    }

    //Returns an array of Workout objects based on a given session ID
    public static function getWorkoutsBySessionID($sessionID) {
        $db = Database::getDB();
        $query = 'SELECT * FROM workout WHERE SessionID = :SessionID ORDER BY workoutID';
        $statement = $db->prepare($query);
        $statement->bindValue(':SessionID', $sessionID);
        $statement->execute();
        $rows = $statement->fetchAll();
        $statement->closeCursor();
        $workouts = array();
        foreach ($rows as $row) {
            $w = Workout::create();
            $w->setID($row['WorkoutID']);
            $w->setExercise(ExerciseDB::getExerciseByID($row['ExerciseID']));
            $w->setReps($row['Reps']);
            $w->setResistanceLevel($row['ResistanceLevel']);
            $w->setSets($row['Sets']);
            $w->setSessionID($row['SessionID']);
            $workouts[] = $w;
        }
        return $workouts;
    }

    //Deletes a given Workout from the database
    public static function deleteWorkout($workoutID) {
        $db = Database::getDB();

        $query = 'DELETE FROM workout WHERE workoutID = :workoutID';
        $statement = $db->prepare($query);
        $statement->bindValue(':workoutID', $workoutID);
        $statement->execute();
        $statement->closeCursor();
    }

    //Adds a given Workout to the database
    public static function addWorkout($workout) {
        $db = Database::getDB();

        $query = 'INSERT INTO workout (ExerciseID, Reps, Sets, ResistanceLevel, SessionID)
                  VALUES (:exerciseID, :reps, :sets, :resistanceLevel, :sessionID)';
        $statement = $db->prepare($query);
        $statement->bindValue(':exerciseID', $workout->getExercise()->getID());
        $statement->bindValue(':reps', $workout->getReps());
        $statement->bindValue(':sets', $workout->getSets());
        $statement->bindValue(':resistanceLevel', $workout->getResistanceLevel());
        $statement->bindValue(':sessionID', $workout->getSessionID());
        $statement->execute();
        $statement->closeCursor();
    }

    //Update an exercise
    public static function updateWorkout($workout) {
        $db = Database::getDB();

        $query = 'UPDATE workout
                  SET ExerciseID = :exerciseID, Sets = :sets, Reps = :reps, ResistanceLevel = :resistanceLevel
                  WHERE WorkoutID = :workoutID';
        $statement = $db->prepare($query);
        $statement->bindValue(':exerciseID', $workout->getExercise()->getID());
        $statement->bindValue(':sets', $workout->getSets());
        $statement->bindValue(':reps', $workout->getReps());
        $statement->bindValue(':resistanceLevel', $workout->getResistanceLevel());
                $statement->bindValue(':workoutID', $workout->getID());
        $statement->execute();
        $statement->closeCursor();
    }

}
