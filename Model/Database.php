<?php
/*
 * Database class that creates a PDO object
 */
class Database {
    private static $dsn = 'mysql:host=localhost;dbname=rep_it_out';
    private static $username = 'root';
    private static $password = '';
    private static $db;

    private function __construct() {}

    public static function getDB () {
        if (!isset(self::$db)) {
            try {
                self::$db = new PDO(self::$dsn,
                                    self::$username,
                                    self::$password);
                self::$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            } catch (PDOException $e) {
                $error_message = $e->getMessage();
                include('errors/db_error.php');
                exit();
            }
        }
        return self::$db;
    }
}
?>
