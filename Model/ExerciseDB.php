<?php

/*
 * Database class that creates Exercise objects from records in the database, deletes Exercise records from the database, and adds Exercise records to the database.
 */

class ExerciseDB {
    //Gets an array of all Exercises in the database
    public static function getExercises() {
        $db = Database::getDB();
        $query = 'SELECT * FROM Exercise ORDER BY exerciseID';
        $statement = $db->prepare($query);
        $statement->execute();
        $rows = $statement->fetchAll();
        $statement->closeCursor();
        $exercises = array();
        foreach ($rows as $row) {
            $e = new Exercise($row['Name']);
            $e->setID($row['ExerciseID']);
            $exercises[] = $e;
        }
        return $exercises;
    }
    
    //Gets one Exercise object for the provided exercise ID
    public static function getExerciseByID($exerciseID) {
        $db = Database::getDB();
        $query = 'SELECT * FROM Exercise WHERE exerciseID = :exerciseID';
        $statement = $db->prepare($query);
        $statement->bindValue(':exerciseID', $exerciseID);
        $statement->execute();
        $row = $statement->fetch();
        $statement->closeCursor();
        $exercise = new Exercise($row['Name']);
        $exercise->setID($row['ExerciseID']);
        return $exercise;
    }

    //Deletes the Exercise record from the database for the provided exercise ID
    public static function deleteExercise($exerciseID) {
        $db = Database::getDB();

        $query = 'DELETE FROM exercise WHERE exerciseID = :exerciseID';
        $statement = $db->prepare($query);
        $statement->bindValue(':exerciseID', $exerciseID);
        $statement->execute();
        $statement->closeCursor();
    }

    //Adds Exercise object to the database
    public static function addExercise($exercise) {
        $db = Database::getDB();

        $query = 'INSERT INTO exercise (Name)
                  VALUES (:name)';
        $statement = $db->prepare($query);
        $statement->bindValue(':name', $exercise->getName());
        $statement->execute();
        $statement->closeCursor();
    }
    
    //Update an exercise
    public static function updateExercise($exercise){
        $db = Database::getDB();

        $query = 'UPDATE exercise
                  SET Name = :name
                  WHERE ExerciseID = :exerciseID';
        $statement = $db->prepare($query);
        $statement->bindValue(':exerciseID', $exercise->getID());
        $statement->bindValue(':name', $exercise->getName());        
        $statement->execute();
        $statement->closeCursor();
    }
    
}
