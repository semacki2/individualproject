<?php

/*
 * Database class that creates User objects from records in the database, deletes User records from the database, and adds User records to the database.
 */

class UserDB {

    //Returns an array of all Users in the database
    public static function getUsers() {
        $db = Database::getDB();
        $query = 'SELECT * FROM user ORDER BY userID';
        $statement = $db->prepare($query);
        $statement->execute();
        $rows = $statement->fetchAll();
        $statement->closeCursor();
        $users = array();
        foreach ($rows as $row) {
            $u = new User($row['Username'], $row['Password'], $row['UserType']);
            $u->setID($row['UserID']);
            $users[] = $u;
        }
        return $users;
    }

    //Returns a User object based on the given user ID
    public static function getUserByID($userID) {
        $db = Database::getDB();
        $query = 'SELECT * FROM user WHERE userID = :userID';
        $statement = $db->prepare($query);
        $statement->bindValue(':userID', $userID);
        $statement->execute();
        $row = $statement->fetch();
        $statement->closeCursor();
        $user = new User($row['Username'], $row['Password'], $row['UserType']);
        $user->setID($row['UserID']);
        return $user;
    }

    //Returns a User object based on the given username
    public static function getUserByUsername($username) {
        $db = Database::getDB();
        $query = 'SELECT * FROM user WHERE username = :username';
        $statement = $db->prepare($query);
        $statement->bindValue(':username', $username);
        $statement->execute();
        $row = $statement->fetch();
        $statement->closeCursor();
        $user = new User($row['Username'], $row['Password'], $row['UserType']);
        $user->setID($row['UserID']);
        return $user;
    }

    //Deletes the given User.
    public static function deleteUser($userID) {
        $db = Database::getDB();

        $query = 'DELETE FROM user WHERE userID = :userID';
        $statement = $db->prepare($query);
        $statement->bindValue(':userID', $userID);
        $statement->execute();
        $statement->closeCursor();
    }

    //Adds a given User object to the database.
    public static function addUser($user) {
        $db = Database::getDB();

        $query = 'INSERT INTO user (Username, Password, UserType)
                  VALUES (:username, :password, :userType)';
        $statement = $db->prepare($query);
        $statement->bindValue(':username', $user->getUsername());
        $statement->bindValue(':password', $user->getPassword());
        $statement->bindValue(':userType', $user->getUserType());
        $statement->execute();
        $statement->closeCursor();
    }

    //Adds a given User object to the database.
    public static function updatePassword($user) {
        $db = Database::getDB();

        $query = 'UPDATE user
                  SET Password = :password
                  WHERE UserID = :userID';
        $statement = $db->prepare($query);
        $statement->bindValue(':userID', $user->getID());
        $statement->bindValue(':password', $user->getPassword());        
        $statement->execute();
        $statement->closeCursor();
    }

}
