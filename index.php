<?php

require_once ('Model/Database.php');
require_once ('Model/Exercise.php');
require_once ('Model/ExerciseDB.php');
require_once ('Model/Session.php');
require_once ('Model/SessionDB.php');
require_once ('Model/User.php');
require_once ('Model/UserDB.php');
require_once ('Model/Workout.php');
require_once ('Model/WorkoutDB.php');
require_once ('Model/Validate.php');
require_once ('Model/Fusioncharts.php');
require_once ('Model/FusionchartsDB.php');

//Start session
session_start();

//Set empty error list array
$errorMessages = [];

//Create newSession as a Session object if it doesn't already exist
if (!isset($_SESSION['newSession']) || !is_a($_SESSION['newSession'], 'Session')) {
    $_SESSION['newSession'] = Session::create();
}

//Get active username for header
if (isset($_SESSION['activeUser'])) {
    $_SESSION['activeUsername'] = $_SESSION['activeUser']->getUsername();
}

//get action/set default action
$action = filter_input(INPUT_POST, 'action');

if ($action === NULL) {
    $action = filter_input(INPUT_GET, 'action');
    if ($action === NULL) {
        if (isset($_SESSION['activeUser'])) {
            $action = 'home';
        } else {
            $action = 'login';
        }
    }
}

switch ($action) {
    //The home page displays a list of workout sessions for that user.
    case 'home':

        //Go to login screen if there isn't an active user in the $_Session
        if (!isset($_SESSION['activeUsername'])) {
            header("Location: .?action=login");
        }

        $user = $_SESSION['activeUser'];
        $sessions = SessionDB::getSessionsByUserID($user->getID());

        include 'views/home.php';
        break;
    //Login page
    case 'login':
        session_unset();
        $username = filter_input(INPUT_POST, 'username');
        $password = filter_input(INPUT_POST, 'password');

        include('views/login.php');
        break;
    //Validate login info. If successful, open the home page for the user; else return to login page
    case 'validateLogin':
        $username = filter_input(INPUT_POST, 'username');
        $password = filter_input(INPUT_POST, 'password');

        if (empty($username)) {
            $errorMessages[] = "Username is required";
        } else if (!Validate::usernameExists($username)) {
            $errorMessages[] = "Username does not exist";
        }

        if (empty($password)) {
            $errorMessages[] = "Password is required";
        } else if (!Validate::passwordIsCorrect($username, $password)) {
            $errorMessages[] = "Password is incorrect";
        }

        //If there is an error, return to the login screen; else, proceed to the home page
        if (Validate::errorIsPresent($errorMessages)) {
            require 'views/login.php';
            die;
        } else {
            $user = UserDB::getUserByUsername($username);
            $_SESSION['activeUser'] = $user;

            header("Location: .?action=home");
        }
        break;
    //Register new user
    case 'register':
        session_unset();
        $username = filter_input(INPUT_POST, 'username');
        $password = filter_input(INPUT_POST, 'password');

        include('views/register.php');
        break;
    //Validate new user registration. 
    case 'validateRegister':
        $username = filter_input(INPUT_POST, 'username');
        $password = filter_input(INPUT_POST, 'password');

        if (empty($username)) {
            $errorMessages[] = "Username is required";
        } else if (strlen($username) > 30 || str_getcsv($username) < 8) {
            $errorMessages[] = "Username must be between 8 and 30 characters";
        } else if (Validate::usernameExists($username)) {
            $errorMessages[] = "Username already exists";
        }

        if (empty($password)) {
            $errorMessages[] = "Password is required";
        } else if (strlen($password) < 8) {
            $errorMessages[] = "Password must be at least 8 characters long";
        }

        if (Validate::errorIsPresent($errorMessages)) {
            require 'views/register.php';
            die;
        } else {
            //Hash the password
            $options = ['cost' => 10];
            $hashedPassword = password_hash($password, PASSWORD_BCRYPT, $options);

            $newUser = new User($username, $hashedPassword, "user");
            UserDB::addUser($newUser);

            $user = UserDB::getUserByUsername($username);
            $_SESSION['activeUser'] = $user;

            header("Location: .?action=home");
        }
        break;
    //Open form to update current user's password
    case 'updatePassword':

        $user = $_SESSION['activeUser'];

        $username = $user->getUsername();
        $password = filter_input(INPUT_POST, 'password');

        include('views/updatePassword.php');
        break;
    //Validate update password form 
    case 'validateUpdatePassword':
        $user = $_SESSION['activeUser'];

        $username = $user->getUsername();
        $password = filter_input(INPUT_POST, 'password');

        if (empty($password)) {
            $errorMessages[] = "Password is required";
        } else if (strlen($password) < 8) {
            $errorMessages[] = "Password must be at least 8 characters long";
        }

        if (Validate::errorIsPresent($errorMessages)) {
            require 'views/updatePassword.php';
            die;
        } else {
            //Hash the password
            $options = ['cost' => 10];
            $hashedPassword = password_hash($password, PASSWORD_BCRYPT, $options);

            $user->setPassword($hashedPassword);
            UserDB::updatePassword($user);

            $user = UserDB::getUserByUsername($username);
            $_SESSION['activeUser'] = $user;

            header("Location: .?action=home");
        }
        break;
    //Opens the form to add a new session
    case 'addSessionForm':

        if (!isset($_SESSION['activeUsername'])) {
            header("Location: .?action=login");
        }

        $newSession = $_SESSION['newSession'];

        if (null !== $newSession->getDate() && $newSession->getDate() != 'undefined') {
            $date = $newSession->getDate();
        }

        if (null !== $newSession->getLength() && $newSession->getLength() != 'undefined') {
            $length = $newSession->getLength();
        }

        if (null !== $newSession->getWorkouts()) {
            $workouts = $newSession->getWorkouts();
        }

        include 'views/addSessionForm.php';
        break;
    //Validate the add session form. If successful, return to the home page; else, return to the form. All form fields are required ****NEED MORE VALIDATION****
    case 'validateAddSession':

        if (!isset($_SESSION['activeUsername'])) {
            header("Location: .?action=login");
        }

        $user = $_SESSION['activeUser'];

        $date = filter_input(INPUT_POST, 'date');
        $length = filter_input(INPUT_POST, 'length');
        $workouts = $_SESSION['newSession']->getWorkouts();

        if (empty($date)) {
            $errorMessages[] = "Date Required";
        } else if (!Validate::isDate($date)) {
            $errorMessages[] = "Invalid Date";
        } else if (Validate::isFutureDate($date)) {
            $errorMessages[] = "Date cannot be in the future";
        }

        if (empty($length)) {
            $errorMessages[] = "Length Required";
        } else if (!Validate::isInteger($length)) {
            $errorMessages[] = "Invalid Length";
        }

        if (null === $workouts) {
            $errorMessages[] = "Workouts Required";
        }

        if (Validate::errorIsPresent($errorMessages)) {
            require 'views/addSessionForm.php';
            die;
        } else {
            $newSession = new Session($date, $length, $user, $workouts);
            $sessionID = SessionDB::addSession($newSession);
            foreach ($workouts as $workout) {
                $workout->setSessionID($sessionID);
                WorkoutDB::addWorkout($workout);
            }

            $_SESSION['newSession'] = Session::create();
            header("Location: .?action=home");
        }

        break;
    //Resets the "newSession" object in the $_Session and returns to the home page
    case 'cancelAddSession':
        $_SESSION['newSession'] = Session::create();

        header("Location: .?action=home");
        break;
    //Open a page to view the selected workout session
    case 'viewSession':
        $sessionID = filter_input(INPUT_POST, 'sessionID');
        $session = SessionDB::getSessionByID($sessionID);
        $workouts = $session->getWorkouts();
        include 'views/viewSession.php';
        break;
    //Delete the selected session
    case 'deleteSession':

        //Go to login screen if there isn't an active user in the $_Session   
        if (!isset($_SESSION['activeUsername'])) {
            header("Location: .?action=login");
        }

        $sessionID = filter_input(INPUT_POST, 'sessionID');

        //Dispaly an error if there isn't a sessionID. This shouldn't happen unless someone is naughty.
        if (empty($sessionID)) {
            $errorMessages[] = "There was an error. Please try again.";
            header("Location: .?action=home");
        } else {
            SessionDB::deleteSession($sessionID);
        }

        header("Location: .?action=home");
        break;
    //Open form to add a new workout to the new session
    case 'addWorkoutToSessionForm':

        if (!isset($_SESSION['activeUsername'])) {
            header("Location: .?action=login");
        }

        //Grab exercises for select box
        $allExercises = ExerciseDB::getExercises();
        $exerciseID = '';

        //update the 'newSession' object's data and length values
        $newSession = $_SESSION['newSession'];
        $date = filter_input(INPUT_POST, 'date');
        $length = filter_input(INPUT_POST, 'length');
        $newSession->setDate($date);
        $newSession->setLength($length);
        $_SESSION['newSession'] = $newSession;

        include 'views/addWorkoutToSessionForm.php';
        break;
    //Validate the add workout form then return to the add session form
    case 'validateAddWorkoutToSession':

        if (!isset($_SESSION['activeUsername'])) {
            header("Location: .?action=login");
        }

        $exerciseID = filter_input(INPUT_POST, 'exercise');
        $sets = filter_input(INPUT_POST, 'sets');
        $reps = filter_input(INPUT_POST, 'reps');
        $resistanceLevel = filter_input(INPUT_POST, 'resistanceLevel');
        $sessionID = filter_input(INPUT_POST, 'sessionID');

        $newSession = $_SESSION['newSession'];

        $user = $_SESSION['activeUser'];

        if (empty($exerciseID)) {
            $errorMessages[] = "Exercise Required";
        }

        if (empty($sets)) {
            $errorMessages[] = "Sets Required";
        } else if (!Validate::isInteger($sets)) {
            $errorMessages[] = "Invalid Sets";
        }

        if (empty($reps)) {
            $errorMessages[] = "Reps Required";
        } else if (!Validate::isInteger($reps)) {
            $errorMessages[] = "Invalid Reps";
        }

        if (empty($resistanceLevel)) {
            $errorMessages[] = "Resistance Level Required";
        } else if (!Validate::isDecimal($resistanceLevel)) {
            $errorMessages[] = "Invalid Resistance Level";
        }

        //If there is an error, return to the form
        //else, if there isn't a sessionID, then add the workout to the new session
        //else, if there is a sessionID, then add it to the old session
        if (Validate::errorIsPresent($errorMessages)) {
            $allExercises = ExerciseDB::getExercises();
            require 'views/addWorkoutToSessionForm.php';
            die;
        } else if (empty($sessionID)) {
            $newWorkout = new Workout(ExerciseDB::getExerciseByID($exerciseID), $sets, $reps, $resistanceLevel);
            $newSession->addWorkoutToSession($newWorkout);
            $_SESSION['newSession'] = $newSession;
            header("Location: .?action=addSessionForm");
        } else if (!empty($sessionID)) {
            $newWorkout = new Workout(ExerciseDB::getExerciseByID($exerciseID), $sets, $reps, $resistanceLevel, $sessionID);
            WorkoutDB::addWorkout($newWorkout);

            $session = SessionDB::getSessionByID($sessionID);
            $workouts = $session->getWorkouts();
            include 'views/viewSession.php';
        }

        break;
    //Open form to add a new workout to an old session
    case 'addWorkoutToOldSession':

        if (!isset($_SESSION['activeUsername'])) {
            header("Location: .?action=login");
        }

        //Grab exercises for select box
        $allExercises = ExerciseDB::getExercises();
        $exerciseID = '';

        $sessionID = filter_input(INPUT_GET, 'sessionID');

        if (empty($sessionID)) {
            $errorMessages[] = 'There was an error. Please try again.';
            header("Location: .?action=home");
        } else {
            include 'views/addWorkoutToSessionForm.php';
        }
        break;
    //Canecels the add new workout form
    case 'cancelAddWorkout':
        $sessionID = filter_input(INPUT_GET, 'sessionID');

        //If there isn't a sessionID, return to the add new session form
        //Else, return to the view session page
        if (empty($sessionID)) {
            header("Location: .?action=addSessionForm");
        } else {
            $session = SessionDB::getSessionByID($sessionID);
            $workouts = $session->getWorkouts();
            include 'views/viewSession.php';
        }
        break;
    //Opens a form for the user to edit the selected workout
    case 'editWorkout':

        if (!isset($_SESSION['activeUsername'])) {
            header("Location: .?action=login");
        }

        //get all exercises for the exercise select box
        $allExercises = ExerciseDB::getExercises();
        
        $exerciseID = filter_input(INPUT_GET, 'exerciseID');
        $sets = filter_input(INPUT_GET, 'sets');
        $reps = filter_input(INPUT_GET, 'reps');
        $resistanceLevel = filter_input(INPUT_GET, 'resistanceLevel');
        
        //index is for modifying workouts that exist in the $_Session['newSession'] object
        $index = filter_input(INPUT_GET, 'index');
        
        //workoutID and sessionID are for modifying workouts that exist in an old session object
        $workoutID = filter_input(INPUT_GET, 'workoutID');
        $sessionID = filter_input(INPUT_GET, 'sessionID');

        $newSession = $_SESSION['newSession'];
        $date = filter_input(INPUT_GET, 'date');
        $length = filter_input(INPUT_GET, 'length');

        $newSession->setDate($date);
        $newSession->setLength($length);

        $_SESSION['newSession'] = $newSession;

        include 'views/editWorkoutForm.php';
        break;
    //Validate the edit workout form. Then return to the add session form
    case 'validateEditWorkout':

        if (!isset($_SESSION['activeUsername'])) {
            header("Location: .?action=login");
        }

        $user = $_SESSION['activeUser'];

        $exerciseID = filter_input(INPUT_POST, 'exercise');
        $sets = filter_input(INPUT_POST, 'sets');
        $reps = filter_input(INPUT_POST, 'reps');
        $resistanceLevel = filter_input(INPUT_POST, 'resistanceLevel');
        $index = filter_input(INPUT_POST, 'index');
        $workoutID = filter_input(INPUT_POST, 'workoutID');
        $sessionID = filter_input(INPUT_POST, 'sessionID');

        $newSession = $_SESSION['newSession'];

        if (empty($exerciseID)) {
            $errorMessages[] = "Exercise Required";
        }

        if (empty($sets)) {
            $errorMessages[] = "Sets Required";
        } else if (!Validate::isInteger($sets)) {
            $errorMessages[] = "Invalid Sets";
        }

        if (empty($reps)) {
            $errorMessages[] = "Reps Required";
        } else if (!Validate::isInteger($reps)) {
            $errorMessages[] = "Invalid Reps";
        }

        if (empty($resistanceLevel)) {
            $errorMessages[] = "Resistance Level Required";
        } else if (!Validate::isDecimal($resistanceLevel)) {
            $errorMessages[] = "Invalid Resistance Level";
        }

        //If there is an error, return to the form
        //Else, if there is not a workoutID, update the workout in the $_Session['newSession'] and return to the add session form
        //Else, if there is a workoutID, update the workout and return to the view session form
        if (Validate::errorIsPresent($errorMessages)) {
            $allExercises = ExerciseDB::getExercises();
            require 'views/editWorkoutForm.php';
            die;
        } else if (empty($workoutID)) {
            $newWorkout = new Workout(ExerciseDB::getExerciseByID($exerciseID), $sets, $reps, $resistanceLevel, null);
            $workouts = $newSession->getWorkouts();
            $workouts[$index] = $newWorkout;
            $newSession->setWorkouts($workouts);
            $_SESSION['newSession'] = $newSession;
            header("Location: .?action=addSessionForm");
        } else if (!empty($workoutID)) {
            $workout = WorkoutDB::getWorkoutByID($workoutID);
            $workout->setExercise(ExerciseDB::getExerciseByID($exerciseID));
            $workout->setSets($sets);
            $workout->setReps($reps);
            $workout->setResistanceLevel($resistanceLevel);
            WorkoutDB::updateWorkout($workout);

            $session = SessionDB::getSessionByID($sessionID);
            $workouts = $session->getWorkouts();
            include 'views/viewSession.php';
        }

        break;
    //Removes the selected workout from the "newSession"
    case 'removeWorkout':

        if (!isset($_SESSION['activeUsername'])) {
            header("Location: .?action=login");
        }

        $newSession = $_SESSION['newSession'];

        $date = filter_input(INPUT_GET, 'date');
        $length = filter_input(INPUT_GET, 'length');
        $index = filter_input(INPUT_GET, 'index');

        $newSession->setDate($date);
        $newSession->setLength($length);

        $_SESSION['newSession'] = $newSession;

        $workouts = $_SESSION['newSession']->getWorkouts();

        array_splice($workouts, $index, 1);

        $_SESSION['newSession']->setWorkouts($workouts);

        include 'views/addSessionForm.php';
        break;
    //Deletes the selected workout
    case 'deleteWorkout':

        if (!isset($_SESSION['activeUsername'])) {
            header("Location: .?action=login");
        }

        $workoutID = filter_input(INPUT_GET, 'workoutID');
        $sessionID = filter_input(INPUT_GET, 'sessionID');

        //Display error if there isn't a workoutID. This should only happen if someone is naughty.
        if (empty($workoutID) || empty($sessionID)) {
            $errorMessages[] = "There was an error. Please try again.";
            header("Location: .?action=home");
        } else {
            $workout = WorkoutDB::deleteWorkout($workoutID);
            $session = SessionDB::getSessionByID($sessionID);
            $workouts = $session->getWorkouts();
            include 'views/viewSession.php';
        }
        break;
        
    //Opens a page for a user to review his/her progression for a selected exercise.
    case 'viewExerciseProgression':

        if (!isset($_SESSION['activeUsername'])) {
            header("Location: .?action=login");
        }

        //Get all exercises for exercise select box.
        $allExercises = ExerciseDB::getExercises();
        $exerciseID = filter_input(INPUT_POST, 'exercise');

        if (empty($exerciseID)) {
            $exerciseID = '';
            $errorMessages[] = "Please select an exercise";
        }

        $user = $_SESSION['activeUser'];
        $userID = $user->getID();

        if (Validate::errorIsPresent($errorMessages)) {
            include 'views/viewExerciseProgression.php';
        } else {
            include 'views/viewExerciseProgression.php';
            FusionchartsDB::generateExerciseChartByUserIDAndExerciseID($userID, $exerciseID);
        }
        break;
    //Open form for admin to change other users' passwords
    case 'manageUsersUpdatePassword':

        //If current user is not set or if current user is not an admin, return to the login screen
        if (!isset($_SESSION['activeUsername'])) {
            header("Location: .?action=login");
        } else if ($_SESSION['activeUser']->getUserType() != 'admin') {
            header("Location: .?action=login");
        }
        
        //Grab users for select box
        $allUsers = UserDB::getUsers();
        $userID = '';

        $password = filter_input(INPUT_POST, 'password');

        include 'views/manageUsersUpdatePassword.php';
        break;
    //Validate the manage users update passwords form then return to the manage users page
    case 'validateManageUsersUpdatePassword':

        //If current user is not set or if current user is not an admin, return to the login screen
        if (!isset($_SESSION['activeUsername'])) {
            header("Location: .?action=login");
        } else if ($_SESSION['activeUser']->getUserType() != 'admin') {
            header("Location: .?action=login");
        }

        $userID = filter_input(INPUT_POST, 'selectUser');
        $user = UserDB::getUserByID($userID);
        $password = filter_input(INPUT_POST, 'password');

        if (empty($userID)) {
            $errorMessages[] = "Please select a user";
        }

        if (empty($password)) {
            $errorMessages[] = "Password is required";
        } else if (strlen($password) < 8) {
            $errorMessages[] = "Password must be at least 8 characters long";
        }

        if (Validate::errorIsPresent($errorMessages)) {
            $allUsers = UserDB::getUsers();
            require 'views/manageUsersUpdatePassword.php';
            die;
        } else {
            //Hash the password
            $options = ['cost' => 10];
            $hashedPassword = password_hash($password, PASSWORD_BCRYPT, $options);

            $user->setPassword($hashedPassword);
            UserDB::updatePassword($user);

            header("Location: .?action=manageUsersUpdatePassword");
        }

        break;
    //Open form for admin to create new user accounts
    case 'manageUsersCreateUser':

        //If current user is not set or if current user is not an admin, return to the login screen
        if (!isset($_SESSION['activeUsername'])) {
            header("Location: .?action=login");
        } else if ($_SESSION['activeUser']->getUserType() != 'admin') {
            header("Location: .?action=login");
        }

        $username = filter_input(INPUT_POST, 'username');
        $password = filter_input(INPUT_POST, 'password');
        $userType = filter_input(INPUT_POST, 'userType');

        include('views/manageUsersCreateUser.php');
        break;
    //Validate manage users create user form
    case 'validateManageUsersCreateUser':

        //If current user is not set or if current user is not an admin, return to the login screen
        if (!isset($_SESSION['activeUsername'])) {
            header("Location: .?action=login");
        } else if ($_SESSION['activeUser']->getUserType() != 'admin') {
            header("Location: .?action=login");
        }

        $username = filter_input(INPUT_POST, 'username');
        $password = filter_input(INPUT_POST, 'password');
        $userType = filter_input(INPUT_POST, 'userType');

        if (empty($username)) {
            $errorMessages[] = "Username is required";
        } else if (strlen($username) > 30 || str_getcsv($username) < 8) {
            $errorMessages[] = "Username must be between 8 and 30 characters";
        } else if (Validate::usernameExists($username)) {
            $errorMessages[] = "Username already exists";
        }

        if (empty($password)) {
            $errorMessages[] = "Password is required";
        } else if (strlen($password) < 8) {
            $errorMessages[] = "Password must be at least 8 characters long";
        }

        if (empty($userType)) {
            $errorMessages[] = "User Type is required";
        }

        if (Validate::errorIsPresent($errorMessages)) {
            require 'views/manageUsersCreateUser.php';
            die;
        } else {
            //Hash the password
            $options = ['cost' => 10];
            $hashedPassword = password_hash($password, PASSWORD_BCRYPT, $options);

            $newUser = new User($username, $hashedPassword, $userType);
            UserDB::addUser($newUser);

            header("Location: .?action=manageUsersCreateUser");
        }

        break;
    //Open form for admin to delete user accounts
    case 'manageUsersDeleteUser':

        //If current user is not set or if current user is not an admin, return to the login screen
        if (!isset($_SESSION['activeUsername'])) {
            header("Location: .?action=login");
        } else if ($_SESSION['activeUser']->getUserType() != 'admin') {
            header("Location: .?action=login");
        }

        //Grab users for select box
        $allUsers = UserDB::getUsers();
        $userID = '';

        include('views/manageUsersDeleteUser.php');
        break;
    //Validate manage users delete user form
    case 'validateManageUsersDeleteUser':

        //If current user is not set or if current user is not an admin, return to the login screen
        if (!isset($_SESSION['activeUsername'])) {
            header("Location: .?action=login");
        } else if ($_SESSION['activeUser']->getUserType() != 'admin') {
            header("Location: .?action=login");
        }

        $userID = filter_input(INPUT_POST, 'selectUser');

        if (empty($userID)) {
            $errorMessages[] = "Please select a user";
        }

        if (Validate::errorIsPresent($errorMessages)) {
            $allUsers = UserDB::getUsers();
            require 'views/manageUsersDeleteUser.php';
            die;
        } else {
            //Hash the password
            UserDB::deleteUser($userID);

            header("Location: .?action=manageUsersDeleteUser");
        }

        break;
    //Opens the manage exercise form for admins to add/edit/delete exercises
    case 'manageExercises':

        //If current user is not set or if current user is not an admin, return to the login screen      
        if (!isset($_SESSION['activeUsername'])) {
            header("Location: .?action=login");
        } else if ($_SESSION['activeUser']->getUserType() != 'admin') {
            header("Location: .?action=login");
        }

        $exercises = ExerciseDB::getExercises();

        include 'views/manageExercises.php';
        break;
    //Opens the form to edit an exercise
    case 'editExercise':

        //If current user is not set or if current user is not an admin, return to the login screen       
        if (!isset($_SESSION['activeUsername'])) {
            header("Location: .?action=login");
        } else if ($_SESSION['activeUser']->getUserType() != 'admin') {
            header("Location: .?action=login");
        }

        $exerciseID = filter_input(INPUT_POST, 'exerciseID');
        $exercise = ExerciseDB::getExerciseByID($exerciseID);
        $exerciseName = $exercise->getName();

        include 'views/editExerciseForm.php';
        break;
    //Validate the edit exercise form
    case 'validateEditExercise':

        //If current user is not set or if current user is not an admin, return to the login screen       
        if (!isset($_SESSION['activeUsername'])) {
            header("Location: .?action=login");
        } else if ($_SESSION['activeUser']->getUserType() != 'admin') {
            header("Location: .?action=login");
        }

        $exerciseID = filter_input(INPUT_POST, 'exerciseID');
        $exerciseName = filter_input(INPUT_POST, 'exerciseName');

        if (empty($exerciseName)) {
            $errorMessages[] = "Exercise name is required";
        }

        if (empty($exerciseID)) {
            $errorMessages[] = "There was an error. Please try again.";
            include 'views/manageExercises.php';
        }

        if (Validate::errorIsPresent($errorMessages)) {
            require 'views/editExerciseForm.php';
            die;
        } else {
            $exercise = new Exercise($exerciseName);
            $exercise->setID($exerciseID);
            ExerciseDB::updateExercise($exercise);
            header("Location: .?action=manageExercises");
        }

        break;
    //Open the form to create a new exercise
    case 'addExercise':

        //If current user is not set or if current user is not an admin, return to the login screen     
        if (!isset($_SESSION['activeUsername'])) {
            header("Location: .?action=login");
        } else if ($_SESSION['activeUser']->getUserType() != 'admin') {
            header("Location: .?action=login");
        }

        $exerciseID = filter_input(INPUT_POST, 'exerciseID');
        $exerciseName = filter_input(INPUT_POST, 'exerciseName');

        include 'views/addExerciseForm.php';
        break;
    //Validate the add exercise form
    case 'validateAddExercise':

        //If current user is not set or if current user is not an admin, return to the login screen      
        if (!isset($_SESSION['activeUsername'])) {
            header("Location: .?action=login");
        } else if ($_SESSION['activeUser']->getUserType() != 'admin') {
            header("Location: .?action=login");
        }

        $exerciseName = filter_input(INPUT_POST, 'exerciseName');

        if (empty($exerciseName)) {
            $errorMessages[] = "Exercise name is required";
        }

        if (Validate::errorIsPresent($errorMessages)) {
            require 'views/addExerciseForm.php';
            die;
        } else {
            $exercise = new Exercise($exerciseName);
            ExerciseDB::addExercise($exercise);
            header("Location: .?action=manageExercises");
        }
    //Delete the selected exercise
    case 'deleteExercise':

        //If current user is not set or if current user is not an admin, return to the login screen     
        if (!isset($_SESSION['activeUsername'])) {
            header("Location: .?action=login");
        } else if ($_SESSION['activeUser']->getUserType() != 'admin') {
            header("Location: .?action=login");
        }

        $exerciseID = filter_input(INPUT_POST, 'exerciseID');

        if (empty($exerciseID)) {
            $errorMessages[] = "There was an error. Please try again.";
            include 'views/manageExercises.php';
        } else {
            ExerciseDB::deleteExercise($exerciseID);
        }

        header("Location: .?action=manageExercises");
        break;
}
?>
